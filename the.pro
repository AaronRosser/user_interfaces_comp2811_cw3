QT += core gui widgets multimedia multimediawidgets

CONFIG += c++11


# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
	dialogs/import_dialog.cpp \
	dialogs/search_dialog.cpp \
	dialogs/edit_dialog.cpp \
	layouts/flow_layout.cpp \
	layouts/layout_base.cpp \
	widgets/control_buttons.cpp \
	widgets/duration_bar.cpp \
	widgets/main_window_widget.cpp \
	widgets/search_dialog_button.cpp \
	widgets/the_button.cpp \
	widgets/the_player.cpp \
	tomeo.cpp \
	widgets/videos_filter_widget.cpp

HEADERS += \
	dialogs/import_dialog.h \
    dialogs/search_dialog.h \
	dialogs/edit_dialog.h \
	layouts/flow_layout.h \
	layouts/layout_base.h \
	widgets/control_buttons.h \
	widgets/duration_bar.h \
	widgets/main_window_widget.h \
	widgets/search_dialog_button.h \
	widgets/the_button.h \
    the_button_info.h \
	widgets/the_player.h \
	widgets/videos_filter_widget.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
