#include "flow_layout.h"

void FlowLayout::setGeometry(const QRect &r)
{
	QLayout::setGeometry(r);

	// Calculate constants
	const int
			kSpacing = 5,
			kMinHeightWidth = 200,
			numColumns = (r.width() - kSpacing) / (kMinHeightWidth + kSpacing),
			width = (r.width() - kSpacing) / numColumns - kSpacing,
			height = width;

	// Loop through widgets
	int numWidgets = 0;
	for (int i = 0; i < list_.size(); i++) {
		// Retrieve widget
		QLayoutItem *o = list_.at(i);
		QWidget *w = getWidget(o);
		if (w == NULL) {
			continue;
		}

		// Calulate grid co-ordinates
		int x = kSpacing + (kSpacing + width) * (numWidgets % numColumns);
		int y = (numWidgets / numColumns) * (height + kSpacing);

		// Set widgets placement and size
		w->setGeometry(x, y, width, height);
		numWidgets++;
	}

	// Set parent widgets height to fit all results
	int numRows = (numWidgets / numColumns) + (numWidgets % numColumns > 0);
	int sectionHeight = numRows * (kSpacing + height) + 2 * kSpacing;
	this->parentWidget()->setFixedHeight(sectionHeight);
}
