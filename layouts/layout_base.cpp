#include "layout_base.h"

#include <QWidget>
#include <iostream>

QWidget *LayoutBase::getWidget(QLayoutItem *item)
{
	try {
		if (item->widget() == NULL) {
			return NULL;
		}
		// Attempt to cast item to responsive widget
		return dynamic_cast<QWidget *>(item->widget());
	}
	catch (std::bad_cast) {
		return NULL;
	}
}

// following methods provide a trivial list-based implementation of the QLayout class
int LayoutBase::count() const {
	return list_.size();
}

QLayoutItem *LayoutBase::itemAt(int idx) const {
	return list_.value(idx);
}

QLayoutItem *LayoutBase::takeAt(int idx) {
	return idx >= 0 && idx < list_.size() ? list_.takeAt(idx) : 0;
}

void LayoutBase::addItem(QLayoutItem *item) {
	list_.append(item);
}

QSize LayoutBase::sizeHint() const {
	return minimumSize();
}

QSize LayoutBase::minimumSize() const {
	return QSize(0, 0);
}

LayoutBase::~LayoutBase() {
	QLayoutItem *item;
	while ((item = takeAt(0))) {
		delete item;
	}
}
