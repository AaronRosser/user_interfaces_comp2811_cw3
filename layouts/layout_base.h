#ifndef LAYOUTBASE_H
#define LAYOUTBASE_H

#include <QList>
#include <QLayout>

class LayoutBase : public QLayout
{
public:
	// standard functions for a QLayout
	virtual void setGeometry(const QRect &rect) override = 0;

	void addItem(QLayoutItem *item) override;
	QSize sizeHint() const override;
	QSize minimumSize() const override;
	int count() const override;
	QLayoutItem *itemAt(int) const override;
	QLayoutItem *takeAt(int) override;

protected:
	LayoutBase() : QLayout() {};
	~LayoutBase();

	QWidget *getWidget(QLayoutItem *item);

	QList<QLayoutItem*> list_;
};

#endif // LAYOUTBASE_H
