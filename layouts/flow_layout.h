#ifndef FLOWLAYOUT_H
#define FLOWLAYOUT_H

#include "layout_base.h"
#include <QWidget>

class FlowLayout : public LayoutBase
{
public:
	FlowLayout() : LayoutBase() {};

	// standard functions for a QLayout
	void setGeometry(const QRect &r) override;
};

#endif // FLOWLAYOUT_H
