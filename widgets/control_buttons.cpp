#include "control_buttons.h"
#include "dialogs/import_dialog.h"

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QSlider>
#include <QLabel>

ControlButtons::ControlButtons(ThePlayer *player, QWidget *parent): QWidget(parent), player(player)
{
    const QSize btnSize = QSize(50, 50);

    QHBoxLayout *l = new QHBoxLayout();
    setLayout(l);

    // Play / pause button
    QPushButton *playPause = new QPushButton();
    playPause->setIcon(QIcon("icons/003-pause-inverted.png"));
    playPause->setFixedSize(btnSize);
    playPause->setIconSize(btnSize);
    connect(playPause, SIGNAL(released()), this, SLOT(handlePlayPause()));
    l->addWidget(playPause);

    // Spacer
    l->addSpacing(btnSize.width());

    // Back
    QPushButton *back = new QPushButton();
    back->setIcon(QIcon("icons/024-previous-inverted.png"));
    back->setFixedSize(btnSize);
    back->setIconSize(btnSize);
    connect(back, SIGNAL(released()), player, SLOT(back()));
    l->addWidget(back);

    // Stop
    QPushButton *stop = new QPushButton();
    stop->setIcon(QIcon("icons/stop.png"));
    stop->setFixedSize(btnSize);
    stop->setIconSize(btnSize);
    connect(stop, SIGNAL(released()), player, SLOT(stop()));
    l->addWidget(stop);

    // Forward
    QPushButton *forward = new QPushButton();
    forward->setIcon(QIcon("icons/023-next-inverted.png"));
    forward->setFixedSize(btnSize);
    forward->setIconSize(btnSize);
    connect(forward, SIGNAL(released()), player, SLOT(forward()));
    l->addWidget(forward);

    // Spacer
    l->addSpacing(btnSize.width());

    // Import
    QPushButton *import = new QPushButton();
    import->setIcon(QIcon("icons/039-playlist-inverted.png"));
    import->setFixedSize(btnSize);
    import->setIconSize(btnSize);
    connect(import, SIGNAL(released()), this, SLOT(handleImport()));
    l->addWidget(import);

    l->addStretch(1);

    // Volume Controls
    QVBoxLayout *volumeVLayout = new QVBoxLayout();

    QSlider *volumeSlider = createVolumeSlider(player);
    volumeVLayout->addWidget(volumeSlider);

    QHBoxLayout *volumeHLayout = new QHBoxLayout();
    volumeHLayout->addWidget(new QLabel("0  "));
    volumeHLayout->addWidget(new QLabel("Volume"));
    volumeHLayout->addWidget(new QLabel("100"));
    volumeVLayout->addLayout(volumeHLayout);

    l->addLayout(volumeVLayout);
}

QSlider * ControlButtons::createVolumeSlider(ThePlayer *player){
	// Initilze slider
    QSlider *volumeSlider = new QSlider();
    volumeSlider->setOrientation(Qt::Orientation::Horizontal);
    volumeSlider->setMinimum(0);
    volumeSlider->setTickInterval(2);
    volumeSlider->setMaximum(100);
    connect(volumeSlider, SIGNAL(valueChanged(int)), player, SLOT(setVolume(int)));

	//set slider colour
	volumeSlider->setStyleSheet("QSlider::handle:horizontal {background-color: #CF811C}");

    return volumeSlider;
}

void ControlButtons::handlePlayPause()
{
	// Toggle play / pause state
	if (player->state() == QMediaPlayer::State::PausedState) {
        player->play();
	}
	else if (player->state() == QMediaPlayer::State::PlayingState) {
        player->pause();
	}
}

void ControlButtons::handleImport()
{
	// Show import dialog and block main window
    ImportDialog *d = new ImportDialog();
    d->exec();
}
