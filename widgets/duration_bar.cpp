#include "duration_bar.h"

#include <QVBoxLayout>
#include <QHBoxLayout>

DurationBar::DurationBar(ThePlayer *player, QWidget *parent) : QWidget(parent), player(player)
{
	QVBoxLayout *l = new QVBoxLayout();
	setLayout(l);

	// Initilize duration bar slider
	slider = new QSlider();
	slider->setOrientation(Qt::Orientation::Horizontal);
	slider->setMinimum(0);
	slider->setTickInterval(1000);
	//set slider colour
	slider->setStyleSheet("QSlider::handle:horizontal {background-color: #CF811C}");
	connect(slider, SIGNAL(sliderPressed()), this, SLOT(sliderPressed()));
	connect(slider, SIGNAL(sliderReleased()), this, SLOT(sliderReleased()));
	l->addWidget(slider);

	QHBoxLayout *hl = new QHBoxLayout();

	// Current time label
	currentTimeStamp = new QLabel("00:00");
	connect(player, SIGNAL(positionChanged(qint64)), this, SLOT(positionChanged(qint64)));
	hl->addWidget(currentTimeStamp);

	hl->addStretch(1);

	// Duration label
	videoDuration = new QLabel("99:99");
	connect(player, SIGNAL(durationChanged(qint64)), this, SLOT(durationChanged(qint64)));
	hl->addWidget(videoDuration);

	l->addLayout(hl);
}

void DurationBar::sliderPressed()
{
	// Save player state and pause upon grabbing slider
	playingState = player->state();
	player->pause();
}

void DurationBar::sliderReleased()
{
	// Set the player position to match the slider
	player->setPosition(slider->value());
	// If was playing before grabbing slider
	if (playingState == QMediaPlayer::State::PlayingState) {
		player->play();
	}
}

void DurationBar::positionChanged(qint64 pos)
{
	slider->setValue(pos);

	int totalSeconds = pos / 1000;

	int minutes = totalSeconds / 60;
	int seconds = totalSeconds - minutes * 60;

	// Format and update current time label
	QString position = QString("%1:%2")
			.arg(minutes, 2, 10, QLatin1Char('0'))
			.arg(seconds, 2, 10, QLatin1Char('0'));
	currentTimeStamp->setText(position);
}

void DurationBar::durationChanged(qint64 dur)
{
	slider->setMaximum(dur);

	int totalSeconds = dur / 1000;

	int minutes = totalSeconds / 60;
	int seconds = totalSeconds - minutes * 60;

	// Format and update duration label
	QString duration = QString("%1:%2")
			.arg(minutes, 2, 10, QLatin1Char('0'))
			.arg(seconds, 2, 10, QLatin1Char('0'));
	videoDuration->setText(duration);
}
