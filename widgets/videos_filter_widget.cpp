#include "videos_filter_widget.h"
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QFormLayout>
#include <QLabel>
#include <QComboBox>

VideosFilterWidget::VideosFilterWidget(QWidget *parent) : QWidget(parent)
{
	setMaximumWidth(800);
	QVBoxLayout *l = new QVBoxLayout();
	setLayout(l);

	l->addWidget(new QLabel("Filter"));
	QHBoxLayout *hl = new QHBoxLayout();

	// Catergory filter
	catComoBox = new QComboBox();
    catComoBox->addItems({   "All",
							 "Sport",
							 "Travelling",
							 "Hobby",
							 "Misc"
					  });
	catComoBox->setMaximumWidth(150);
	connect(catComoBox, SIGNAL(currentIndexChanged(int)), this, SLOT(filterChanged()));

	QFormLayout *catForm = new QFormLayout();
	catForm->addRow(new QLabel("Catergory"), catComoBox);
	hl->addLayout(catForm);

	// Min time filter
	minTimeComoBox = new QComboBox();
    minTimeComoBox->addItems({   "-",
								 "10 seconds",
								 "1 minute",
								 "5 minutes",
								 "10 minutes",
								 "30 minutes",
								 "1 hour"
							 });
	minTimeComoBox->setMaximumWidth(150);
	connect(minTimeComoBox, SIGNAL(currentIndexChanged(int)), this, SLOT(filterChanged()));

	QFormLayout *minTimeForm = new QFormLayout();
	minTimeForm->addRow(new QLabel("Min Time"), minTimeComoBox);
	hl->addLayout(minTimeForm);

	// Max time filter
	maxTimeComoBox = new QComboBox();
    maxTimeComoBox->addItems({   "-",
								 "10 seconds",
								 "1 minute",
								 "5 minutes",
								 "10 minutes",
								 "30 minutes",
								 "1 hour"
							 });
	maxTimeComoBox->setMaximumWidth(150);
	connect(maxTimeComoBox, SIGNAL(currentIndexChanged(int)), this, SLOT(filterChanged()));

	QFormLayout *maxTimeForm = new QFormLayout();
	maxTimeForm->addRow(new QLabel("Max Time"), maxTimeComoBox);
	hl->addLayout(maxTimeForm);

	l->addLayout(hl);
}

int VideosFilterWidget::timeIndexToSecs(int i)
{
	if (i == 0) {
		return -1;			// Unselected
	} else if (i == 1) {
		return 10;			// 10 secs
	} else if (i == 2) {
		return 60;			// 1  minute
	} else if (i == 3) {
		return 60 * 5;		// 10 minutes
	} else if (i == 4) {
		return 60 * 10;		// 10 minutes
	} else if (i == 5) {
		return 60 * 30;		// 30 minutes
	} else if (i == 6) {
		return 60 * 60 * 1;	// 1  hour
	}

	return -1;
}

void VideosFilterWidget::filterChanged()
{
	// Emit filter changed signal
	QString cat = catComoBox->currentText();
	int minTimeSecs = timeIndexToSecs(minTimeComoBox->currentIndex());
	int maxTimeSecs = timeIndexToSecs(maxTimeComoBox->currentIndex());
	emit filterUpdated(cat, minTimeSecs, maxTimeSecs);
}
