#ifndef CONTROLBAR_H
#define CONTROLBAR_H

#include "the_player.h"

#include <QWidget>
#include <QSlider>

class ControlButtons : public QWidget
{
	Q_OBJECT
public:
	explicit ControlButtons(ThePlayer *player, QWidget *parent = nullptr);
    QSlider *createVolumeSlider(ThePlayer *player);

private:
	ThePlayer *player;

signals:

private slots:
	void handlePlayPause();
	void handleImport();
};

#endif // CONTROLBAR_H
