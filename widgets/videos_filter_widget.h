#ifndef VIDEOSFILTERWIDGET_H
#define VIDEOSFILTERWIDGET_H

#include <QWidget>
#include <QComboBox>

class VideosFilterWidget : public QWidget
{
	Q_OBJECT
public:
	explicit VideosFilterWidget(QWidget *parent = nullptr);

private:
	int timeIndexToSecs(int i);
	QComboBox *catComoBox;
	QComboBox *minTimeComoBox;
	QComboBox *maxTimeComoBox;

private slots:
	void filterChanged();

signals:
	void filterUpdated(QString cat, int minTimeSecs, int maxTimeSecs);

};

#endif // VIDEOSFILTERWIDGET_H
