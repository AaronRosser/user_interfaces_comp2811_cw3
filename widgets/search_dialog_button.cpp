#include "search_dialog_button.h"
#include "dialogs/edit_dialog.h"
#include <QVBoxLayout>

SearchDialogButton::SearchDialogButton(QWidget *parent) : QWidget(parent)
{
    QVBoxLayout *btnArea = new QVBoxLayout();

    // Create init and centre video button
    QHBoxLayout *hCentre = new QHBoxLayout();
    vidBtn = new TheButton(this);
    hCentre->addWidget(vidBtn);
    btnArea->addLayout(hCentre);

    // Create edit button
    QPushButton *editBtn = new QPushButton("Edit");
    connect(editBtn, SIGNAL(released()), this, SLOT(editBtnReleased()));
    btnArea->addWidget(editBtn);

    // Create delete button
    QPushButton *deleteBtn = new QPushButton("Delete");
    btnArea->addWidget(deleteBtn);

    btnArea->addStretch(1);
    setLayout(btnArea);
}

void SearchDialogButton::init(TheButtonInfo *i)
{
	// Initilize underlying button
    vidBtn->init(i);
}

void SearchDialogButton::editBtnReleased()
{
	// Show edit dialog and block main window
    EditDialog d(vidBtn->info->url);
    d.exec();
}
