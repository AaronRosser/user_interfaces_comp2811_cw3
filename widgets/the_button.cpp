//
// Created by twak on 11/11/2019.
//

#include "the_button.h"
#include <QFile>

TheButton::TheButton(QWidget *parent) :  QToolButton(parent) {
	setIconSize(QSize(208, 117));
	connect(this, SIGNAL(released()), this, SLOT(clicked()));
	setStyleSheet("background-color: black; color: white");

	// Place video title under picture
	setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
}

void TheButton::init(TheButtonInfo* i) {
    setIcon( *(i->icon) );
	info = i;

	setText(i->url->fileName());
}

void TheButton::clicked() {
    emit jumpTo(info);
}
