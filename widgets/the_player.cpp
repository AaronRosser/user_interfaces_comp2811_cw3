//
// Created by twak on 11/11/2019.
//

#include "the_player.h"

using namespace std;

ThePlayer::ThePlayer() : QMediaPlayer(NULL) {
	// Disable sound on start up
	setVolume(0);
}

// all buttons have been setup, store pointers here
void ThePlayer::setContent(vector<TheButton*>* b, vector<TheButtonInfo>* i) {
    buttons = b;
    infos = i;
    jumpTo(buttons -> at(0) -> info);
	currentBtnIndex = 0;
}

void ThePlayer::playStateChanged (QMediaPlayer::State ms) {
	if (ms == QMediaPlayer::State::StoppedState) {
		play();
	}
}

void ThePlayer::jumpTo (TheButtonInfo *button) {
	setMedia(*button->url);
	play();

	// Find button index
	auto it = buttons->begin();
	while (it != buttons->end()) {
		if ((*it)->info == button) {
			currentBtnIndex = it - buttons->begin();
			break;
		}
		it++;
	}
	if (it == buttons->end()) {
		currentBtnIndex = -1;
	}
}

void ThePlayer::back()
{
	if (currentBtnIndex == -1ul || currentBtnIndex > buttons->size()) {
		jumpTo(buttons->at(0)->info);
		return;
	}

	if (currentBtnIndex == 0) {
		jumpTo(buttons->back()->info);
		return;
	}

	jumpTo(buttons->at(currentBtnIndex - 1)->info);
}

void ThePlayer::forward()
{
	if (currentBtnIndex == -1ul || currentBtnIndex >= buttons->size() - 1) {
		jumpTo(buttons->at(0)->info);
		return;
	}

	jumpTo(buttons->at(currentBtnIndex + 1)->info);
}
