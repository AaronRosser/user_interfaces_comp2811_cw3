#include "main_window_widget.h"

#include "control_buttons.h"
#include "duration_bar.h"
#include "videos_filter_widget.h"

#include <QVideoWidget>
#include <QVBoxLayout>
#include <QScrollBar>
#include <QMessageBox>
#include <QLineEdit>

MainWindowWidget::MainWindowWidget(QWidget *parent) : QWidget(parent)
{
	buttons = new vector<TheButton*>;

	// create the main window and layout
	QVBoxLayout *top = new QVBoxLayout();

	//set the color of the window
	QPalette pal = palette();
	pal.setColor(QPalette::Background, QColor(125,177,193));
	setPalette(pal);

	setAutoFillBackground(true);
	setLayout(top);
	setWindowTitle("tomeo");
	setMinimumSize(800, 680);

	// Video player
	QVideoWidget *videoWidget = new QVideoWidget;
	player = new ThePlayer;
	player->setVideoOutput(videoWidget);
	top->addWidget(videoWidget);

	top->addWidget(new DurationBar(player));
	top->addWidget(new ControlButtons(player));

	// Filter Widget
	VideosFilterWidget *filterWidget = new VideosFilterWidget();
	top->addWidget(filterWidget);

	// Search dialog button
	QPushButton *search = new QPushButton("Search and Edit Videos");
	connect(search, SIGNAL(released()), this, SLOT(handleSearchBtn()));
    top->addWidget(search);

    top->addWidget(createBtnsScrollArea());
}

void MainWindowWidget::handleSearchBtn()
{
	SearchDialog *searchPage = new SearchDialog(videos);
	searchPage->exec();
}


MainWindowWidget::~MainWindowWidget()
{
	delete buttons;
}

void MainWindowWidget::setVideos(vector<TheButtonInfo> *videos)
{
	this->videos = videos;
}

QScrollArea *MainWindowWidget::createBtnsScrollArea()
{
	// Container for video thumbnail widgets
    QScrollArea *btnsScrollWidget = new QScrollArea();
    btnsScrollWidget->setWidgetResizable(true);
    btnsScrollWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    btnsScrollWidget->horizontalScrollBar()->setEnabled(true);
	btnsScrollWidget->setFixedHeight(190);

	btnsContainer = new QWidget();

    btnsScrollWidget->setWidget(btnsContainer);
    return btnsScrollWidget;
}

void MainWindowWidget::createVideoBtns()
{
	QHBoxLayout *btnsLayout = new QHBoxLayout();

	//determine how many buttons to create
	int numBtns = (int) videos->size();

	// create the buttons
	for (int i = 0; i < numBtns; i++ ) {
		TheButton *button = new TheButton(btnsContainer);
        // when clicked, tell the player to play.
        button->connect(button, SIGNAL(jumpTo(TheButtonInfo* )), player, SLOT (jumpTo(TheButtonInfo*)));
		button->init(&videos->at(i));

		buttons->push_back(button);
		btnsLayout->addWidget(button);
	}
	btnsLayout->addStretch(1);

	btnsContainer->setLayout(btnsLayout);
	// tell the player what buttons and videos are available
	player->setContent(buttons, videos);
}
