#ifndef DURATIONBAR_H
#define DURATIONBAR_H

#include "the_player.h"
#include <QWidget>
#include <QLabel>
#include <QSlider>
#include <QMediaPlayer>

class DurationBar : public QWidget
{
	Q_OBJECT
public:
	explicit DurationBar(ThePlayer *player, QWidget *parent = nullptr);

private:
	ThePlayer *player;
	QSlider *slider;
	QLabel *currentTimeStamp;
	QLabel *videoDuration;
	QMediaPlayer::State playingState;

signals:

private slots:
	void sliderPressed();
	void sliderReleased();
	void positionChanged(qint64 pos);
	void durationChanged(qint64 dur);

};

#endif // DURATIONBAR_H
