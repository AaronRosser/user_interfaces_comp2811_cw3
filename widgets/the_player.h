//
// Created by twak on 11/11/2019.
//

#ifndef CW2_THE_PLAYER_H
#define CW2_THE_PLAYER_H


#include <QApplication>
#include <QMediaPlayer>
#include "the_button.h"
#include <vector>
#include <QTimer>

using namespace std;

class ThePlayer : public QMediaPlayer {

Q_OBJECT

private:
	vector<TheButtonInfo> *infos;
	vector<TheButton*> *buttons;
    QTimer* mTimer;
    long updateCount = 0;
	ulong currentBtnIndex = 0;

public:
	ThePlayer();
	void setContent(vector<TheButton*> *b, vector<TheButtonInfo> *i);

private slots:
    void playStateChanged (QMediaPlayer::State ms);

public slots:
    void jumpTo (TheButtonInfo* button);
	void back();
	void forward();
};

#endif //CW2_THE_PLAYER_H
