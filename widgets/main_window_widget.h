#ifndef MAINWINDOWWIDGET_H
#define MAINWINDOWWIDGET_H

#include "the_button_info.h"
#include "the_player.h"
#include "the_button.h"
#include "dialogs/search_dialog.h"

#include <stdlib.h>
#include <QWidget>
#include <QScrollArea>

using namespace std;

class MainWindowWidget : public QWidget
{
	Q_OBJECT
public:
	explicit MainWindowWidget(QWidget *parent = nullptr);
	~MainWindowWidget();

	void setVideos(vector<TheButtonInfo> *videos);
	void createVideoBtns();

private:
    QScrollArea *createBtnsScrollArea();

	ThePlayer *player;
	vector<TheButton*> *buttons;
	QWidget *btnsContainer;
	vector<TheButtonInfo> *videos;

signals:

private slots:
	void handleSearchBtn();
};

#endif // MAINWINDOWWIDGET_H
