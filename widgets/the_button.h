//
// Created by twak on 11/11/2019.
//

#ifndef CW2_THE_BUTTON_H
#define CW2_THE_BUTTON_H

#include "the_button_info.h"
#include <QPushButton>
#include <QUrl>

#include <QToolButton>

class TheButton : public QToolButton {
    Q_OBJECT

public:
	TheButtonInfo* info;
	TheButton(QWidget *parent);
	void init(TheButtonInfo* i);

private slots:
    void clicked();

signals:
    void jumpTo(TheButtonInfo*);

};

#endif //CW2_THE_BUTTON_H
