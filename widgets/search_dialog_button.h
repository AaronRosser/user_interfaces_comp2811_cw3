#ifndef SEARCHDIALOGBUTTON_H
#define SEARCHDIALOGBUTTON_H

#include "the_button_info.h"
#include "the_button.h"
#include <QWidget>

class SearchDialogButton : public QWidget
{
    Q_OBJECT
public:
    explicit SearchDialogButton(QWidget *parent = nullptr);
    void init(TheButtonInfo* i);

private:
    TheButton *vidBtn;

signals:

private slots:
    void editBtnReleased();

};

#endif // SEARCHDIALOGBUTTON_H
