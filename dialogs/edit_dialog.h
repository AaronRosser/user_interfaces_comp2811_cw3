#ifndef EDITDIALOG_H
#define EDITDIALOG_H

#include <QDialog>

#include <QPushButton>
#include <QLineEdit>
#include <QTextEdit>
#include <QComboBox>
#include <QLabel>

class EditDialog : public QDialog
{
    Q_OBJECT
public:
    EditDialog(QUrl *filePath);

private:
    void setWidgetsEnabled(bool enabled);

    QLineEdit *filePathTextEdit;
    QLineEdit *title;
    QComboBox *cat;
    QLabel *videoDuration;
    QTextEdit *tags;
    QPushButton *saveBtn;

private slots:
    void handleSelectFileBtn();
    void filePathChanged(const QString &path);
};

#endif // EDITDIALOG_H
