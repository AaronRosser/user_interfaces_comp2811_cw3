#ifndef IMPORTDIALOG_H
#define IMPORTDIALOG_H

#include <QDialog>

#include <QPushButton>
#include <QLineEdit>
#include <QTextEdit>
#include <QComboBox>
#include <QLabel>

class ImportDialog : public QDialog
{
	Q_OBJECT
public:
	ImportDialog();

private:
	void setWidgetsEnabled(bool enabled);

	QLineEdit *filePathTextEdit;
	QLineEdit *title;
	QComboBox *cat;
	QLabel *videoDuration;
	QTextEdit *tags;
	QPushButton *importBtn;

private slots:
	void handleSelectFileBtn();
	void filePathChanged(const QString &path);
};

#endif // IMPORTDIALOG_H
