#include "search_dialog.h"
#include "layouts/flow_layout.h"
#include "widgets/search_dialog_button.h"

SearchDialog::SearchDialog(vector<TheButtonInfo> *videoVector){
    setWindowTitle("Search Videos");
    setMinimumSize(500, 500);

    //set the color of the window
    QPalette pal = palette();
    pal.setColor(QPalette::Background, QColor(125,177,193));
    setPalette(pal);

    //create the videos vector
    videos = videoVector;

    // Add widgets to page
    searchPage = new QVBoxLayout;

    QHBoxLayout *searchBox = new QHBoxLayout();

    // Search line edit
    searchBar = new QLineEdit();
    searchBox->addWidget(searchBar);

    // Search button
    QPushButton *searchButton = new QPushButton("Search");
    connect(searchButton, &QPushButton::released, this, &SearchDialog::search);
    searchBox->addWidget(searchButton);

    searchPage->addLayout(searchBox);

	// Add and setup scroll area
    btnsScrollWidget = new QScrollArea();
    btnsScrollWidget->setWidgetResizable(true);
    btnsScrollWidget->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    btnsScrollWidget->verticalScrollBar()->setEnabled(true);
    btnsScrollWidget->setMinimumHeight(130);

    btnsScrollWidget->setWidget(createButtons(""));

    searchPage->addWidget(btnsScrollWidget);

    setLayout(searchPage);
}

QWidget *SearchDialog::createButtons(QString searchStr)
{
    // Layout to put the thumbnails into a grid formation
    QWidget *btnsLayoutWidget = new QWidget();
    FlowLayout *btnsLayout = new FlowLayout();

    //determine how many buttons to create
    int buttonNum = (int) videos->size();

    // create the buttons
    for ( int i = 0; i < buttonNum; i++ ) {
        //check if current video is the one user is searching for
        QString videoTitle = videos->at(i).url->fileName();
		if(videoTitle.indexOf(searchStr) == -1) {
            continue;
		}

		// Initilize a video button
        SearchDialogButton *btn = new SearchDialogButton();
        btn->init(&videos->at(i));
        btnsLayout->addWidget(btn);
    }

    btnsLayoutWidget->setLayout(btnsLayout);
    return btnsLayoutWidget;
}

void SearchDialog::search(){
    // Check if previous search results are being shown and remove them
    QWidget *allVids = btnsScrollWidget->widget();
	if(allVids) {
        allVids->deleteLater();
	}

	// Update the search results
    btnsScrollWidget->setWidget(createButtons(searchBar->text()));
}
