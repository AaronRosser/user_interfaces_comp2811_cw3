#ifndef SEARCH_DIALOG_H
#define SEARCH_DIALOG_H


#include <QDialog>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QScrollArea>
#include <QScrollBar>
#include <QPushButton>
#include "widgets/the_button.h"
#include <vector>

using namespace std;



class SearchDialog : public QDialog
{
    Q_OBJECT
public:
    SearchDialog(vector<TheButtonInfo> *videoVector);
    void search();
    QWidget *createButtons(QString searchStr);

private:
	QLineEdit *searchBar;
	QVBoxLayout *searchPage;
    vector<TheButton*> *buttons;
    vector<TheButtonInfo> *videos;
    QScrollArea *btnsScrollWidget;
};

#endif // SEARCH_DIALOG_H
