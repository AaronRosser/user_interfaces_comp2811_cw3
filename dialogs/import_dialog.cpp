#include "import_dialog.h"

#include <QFormLayout>
#include <QFileDialog>

ImportDialog::ImportDialog()
{
	setWindowTitle("Import Video");
	setMinimumSize(400, 400);

	//set the color of the window
	QPalette pal = palette();
	pal.setColor(QPalette::Background, QColor(125,177,193));
	setPalette(pal);

	QVBoxLayout *l = new QVBoxLayout();
	QFormLayout *fLayout = new QFormLayout();

	// File location
	QHBoxLayout *fileLocPathLayout = new QHBoxLayout;

	// File path textbox
	filePathTextEdit = new QLineEdit();
	connect(filePathTextEdit, SIGNAL(textChanged(QString)), this, SLOT(filePathChanged(QString)));
	fileLocPathLayout->addWidget(filePathTextEdit);

	// Select file button
	QPushButton *selectFileBtn = new QPushButton("Select");
	connect(selectFileBtn, SIGNAL(released()), this, SLOT(handleSelectFileBtn()));
	fileLocPathLayout->addWidget(selectFileBtn);

	fLayout->addRow(new QLabel("File Location:"), fileLocPathLayout);

	// Bad spacer
	fLayout->addWidget(new QLabel(""));

	// Selected file section
	QLabel *selectedFileLabel = new QLabel("Selected File:");
	selectedFileLabel->setFont(QFont("Arial", 12, QFont::Bold));
	fLayout->addRow(selectedFileLabel, new QWidget());

	// Title
	title = new QLineEdit();
	fLayout->addRow(new QLabel("Title:"), title);

	// Catergory
	cat = new QComboBox();
	cat->addItems({
					  "Misc",
					  "Sport",
					  "Travelling",
					  "Hobby"
				  });
	fLayout->addRow(new QLabel("Catergory:"), cat);

	// Duration
	videoDuration = new QLabel("00:00");
	fLayout->addRow(new QLabel("Duration:"), videoDuration);
	l->addLayout(fLayout);

	// Tags
	l->addWidget(new QLabel("Tags (comma seperated)"));
	tags = new QTextEdit();
	l->addWidget(tags);

	// Import / Cancel buttons
	QHBoxLayout *buttonsLayout = new QHBoxLayout();

	// Cancel button
	QPushButton *cancelBtn = new QPushButton("Cancel");
	connect(cancelBtn, SIGNAL(released()), this, SLOT(reject()));
	buttonsLayout->addWidget(cancelBtn);

	// Import button
	importBtn = new QPushButton("Import");
	connect(importBtn, SIGNAL(released()), this, SLOT(accept()));
	buttonsLayout->addWidget(importBtn);

	l->addLayout(buttonsLayout);

	setWidgetsEnabled(false);
	setLayout(l);
}

void ImportDialog::setWidgetsEnabled(bool enabled)
{
	title->setEnabled(enabled);
	cat->setEnabled(enabled);
	tags->setEnabled(enabled);
	importBtn->setEnabled(enabled);
}

void ImportDialog::handleSelectFileBtn()
{
	QString filePath = QFileDialog::getOpenFileName(this,
		tr("Open Video"), "", tr("Video Files (*.wmv *.mp4 *.MOV)"));

	filePathTextEdit->setText(filePath);
}

void ImportDialog::filePathChanged(const QString &path)
{
	if (!QFile::exists(path)) {
		setWidgetsEnabled(false);
		// Reset form data
		title->setText("");
		cat->setCurrentIndex(0);
		videoDuration->setText("00:00");
		tags->setText("");
		return;
	}

	// Fill out form with example information
	QUrl u(QUrl::fromLocalFile(path));
	title->setText(u.fileName());
	cat->setCurrentIndex(1);
	videoDuration->setText("02:37");
	tags->setText("action,france,holiday");

	setWidgetsEnabled(true);
}
