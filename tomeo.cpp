/*
 *
 *    ______
 *   /_  __/___  ____ ___  ___  ____
 *    / / / __ \/ __ `__ \/ _ \/ __ \
 *   / / / /_/ / / / / / /  __/ /_/ /
 *  /_/  \____/_/ /_/ /_/\___/\____/
 *              video for sports enthusiasts...
 *
 *  2811 cw3 : twak
 */

#include "the_button_info.h"
#include "widgets/main_window_widget.h"

#include <QApplication>

// Getting videos files
#include <vector>
#include <QtCore/QFileInfo>
#include <QtWidgets/QFileIconProvider>
#include <QtCore/QDir>
#include <QtCore/QDirIterator>
#include <QImageReader>

// Show download prompt / Open URL
#include <QMessageBox>
#include <QDesktopServices>

#if defined(_WIN32)
// Windows
#define VIDEO_EXTENTION_1 ".wmv"
#define VIDEO_EXTENTION_2 ".wmv"
#else
// Mac / Linux
#define VIDEO_EXTENTION_1 ".mp4"
#define VIDEO_EXTENTION_2 ".MOV"
#endif

using namespace std;

// Read in videos and thumbnails to directory
vector<TheButtonInfo> getInfoIn (string loc) {
    vector<TheButtonInfo> out =  vector<TheButtonInfo>();

    QDir dir(QString::fromStdString(loc) );
    QDirIterator it(dir);

	// Loop over all video files
	while (it.hasNext()) {
		QString f = it.next();

		if (!f.contains(".") || !(f.contains(VIDEO_EXTENTION_1) || f.contains(VIDEO_EXTENTION_2))) {
			continue;
		}

		QString thumb = f.left( f .length() - 4) +".png";
		// If a png thumbnail does not exist
		if (!QFile(thumb).exists()) {
			qDebug() << "warning: skipping video because I couldn't find thumbnail " << thumb << Qt::endl;
			continue;
		}

		QImageReader *imageReader = new QImageReader(thumb);

		QImage sprite = imageReader->read(); // read the thumbnail
		if (sprite.isNull()) {
			qDebug() << "warning: skipping video because I couldn't process thumbnail " << thumb << Qt::endl;
			continue;
		}

		QIcon* ico = new QIcon(QPixmap::fromImage(sprite)); // voodoo to create an icon for the button
		QUrl* url = new QUrl(QUrl::fromLocalFile( f )); // convert the file location to a generic url
		out . push_back(TheButtonInfo( url , ico  ) ); // add to the output list
    }

    return out;
}

void downloadNewVideosDialog() {
    const int result = QMessageBox::question(
                NULL,
                QString("Tomeo"),
                QString("You are currently using the old video file from \"quoted\" "
                        "file location. Download new videos from Ramat's OneDrive?"),
                QMessageBox::Yes |
                QMessageBox::No );

	if (result == QMessageBox::Yes) {
        QDesktopServices::openUrl(QUrl("https://leeds365-my.sharepoint.com/:f:/g/personal/"
                                       "sc19ra_leeds_ac_uk/EmRuB5hngMxIsoSeOik5JBABKMSAzRenjZ4Ee1L4bTMXww?e=uDEMuK"));
	}
    exit(-1);
}

void downloadVideosDialog() {
	const int result = QMessageBox::question(
				NULL,
				QString("Tomeo"),
                QString("no videos found! download, unzip, and add command line argument to \"quoted\" "
                        "file location. Download videos from Tom's OneDrive?"),
				QMessageBox::Yes |
				QMessageBox::No );

	if (result == QMessageBox::Yes) {
		QDesktopServices::openUrl(QUrl("https://leeds365-my.sharepoint.com/:f:/g/personal/"
									   "sc19ra_leeds_ac_uk/EmRuB5hngMxIsoSeOik5JBABKMSAzRenjZ4Ee1L4bTMXww?e=uDEMuK"));
	}
	exit(-1);
}

int main(int argc, char *argv[]) {
	qDebug() << "Qt version: " << QT_VERSION_STR << Qt::endl;
    QApplication app(argc, argv);

	vector<TheButtonInfo> videos;

	// Get the videos
	if (argc == 2) {
		videos = getInfoIn(string(argv[1]));
	}

	if(videos.size() == 4) {
        downloadNewVideosDialog();
	}

	if (videos.size() == 0) {
		downloadVideosDialog();
	}

	MainWindowWidget w;
	w.setVideos(&videos);
	w.createVideoBtns();

	w.show();

    // wait for the app to terminate
    return app.exec();
}
