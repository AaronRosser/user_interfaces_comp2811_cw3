#ifndef THE_BUTTON_INFO_H
#define THE_BUTTON_INFO_H

#include <QUrl>
#include <QIcon>

class TheButtonInfo {

public:
	QUrl* url; // video file to play
	QIcon* icon; // icon to display
	TheButtonInfo ( QUrl* url, QIcon* icon) : url (url), icon (icon) {}
};

#endif // THE_BUTTON_INFO_H
